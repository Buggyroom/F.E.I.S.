#pragma once

#include <SFML/Graphics/Color.hpp>

namespace colors {
    const sf::Color green = {77, 214, 20};
    const sf::Color cyan = {102, 205, 255};
    const sf::Color grey = {127, 127, 127};
}