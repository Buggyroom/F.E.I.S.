#pragma once

#include <cstddef>
#include <optional>

#include <rapidcheck.h>
#include <rapidcheck/gen/Arbitrary.h>
#include <rapidcheck/gen/Build.h>
#include <rapidcheck/gen/Container.h>
#include <rapidcheck/gen/Exec.h>
#include <rapidcheck/gen/Numeric.h>
#include <rapidcheck/gen/Predicate.h>
#include <rapidcheck/gen/Transform.h>

#include "../../better_chart.hpp"
#include "../../better_hakus.hpp"
#include "../../better_metadata.hpp"
#include "../../better_note.hpp"
#include "../../better_notes.hpp"
#include "../../better_song.hpp"
#include "../../better_timing.hpp"
#include "../../variant_visitor.hpp"
#include "rapidcheck/gen/Arbitrary.hpp"


namespace rc {
    template<>
    struct Arbitrary<better::Position> {
        static Gen<better::Position> arbitrary() {
            return gen::construct<better::Position>(
                gen::inRange<unsigned int>(0, 16)
            );
        }
    };

    template<>
    struct Arbitrary<Fraction> {
        static Gen<Fraction> arbitrary() {
            return gen::apply([](const Fraction& a, unsigned int b, unsigned int c) {
                    return a + Fraction{std::min(b, c), std::max(b, c)};
                },
                gen::construct<Fraction>(gen::inRange<int>(-100,100)),
                gen::inRange<int>(0,10),
                gen::inRange<int>(1,10)
            );
        }
    };

    template <>
    Gen<Fraction> gen::nonNegative();

    template <>
    Gen<Fraction> gen::positive();

    template<>
    struct Arbitrary<better::TapNote> {
        static Gen<better::TapNote> arbitrary() {
            return gen::construct<better::TapNote>(
                gen::nonNegative<Fraction>(),
                gen::arbitrary<better::Position>()
            );
        }
    };

    template<>
    struct Arbitrary<better::LongNote> {
        static Gen<better::LongNote> arbitrary() {
            return gen::apply(
                [](
                    const Fraction& time,
                    const better::Position& position,
                    const Fraction& duration,
                    unsigned int tail_6_notation
                ){
                    const auto tail_tip = better::convert_6_notation_to_position(
                        position, tail_6_notation
                    );
                    return better::LongNote{
                        time,
                        position,
                        duration,
                        tail_tip,
                    };
                },
                gen::nonNegative<Fraction>(),
                gen::arbitrary<better::Position>(),
                gen::positive<Fraction>(),
                gen::inRange<unsigned int>(0, 6)
            );
        };
    };

    template<>
    struct Arbitrary<better::Note> {
        static Gen<better::Note> arbitrary() {
            return gen::oneOf(
                gen::construct<better::Note>(gen::arbitrary<better::TapNote>()),
                gen::construct<better::Note>(gen::arbitrary<better::LongNote>())
            );
        }
    };

    template<>
    struct Arbitrary<better::Notes> {
        static Gen<better::Notes> arbitrary() {
            return gen::exec([](){
                const auto raw_notes = *gen::container<std::vector<std::tuple<better::Position, Fraction, Fraction>>>(gen::tuple(
                    gen::arbitrary<better::Position>(),
                    gen::positive<Fraction>(),
                    gen::nonNegative<Fraction>()
                ));
                std::array<Fraction, 16> last_note_end = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
                better::Notes result;
                for (const auto& [position, delay, duration]: raw_notes) {
                    if (duration > 0) {
                        const auto tail_6_notation = *gen::inRange<unsigned int>(0, 6);
                        const auto tail_tip = better::convert_6_notation_to_position(position, tail_6_notation);
                        auto& end = last_note_end[position.index()];
                        const auto time = end + delay;
                        end += delay + duration;
                        result.insert(better::LongNote{time, position, duration, tail_tip});
                    } else {
                        auto& end = last_note_end[position.index()];
                        const auto time = end + delay;
                        end += delay;
                        result.insert(better::TapNote{time, position});
                    }
                }
                return result;
            });
        }; 
    };

    template<>
    struct Arbitrary<Decimal> {
        static Gen<Decimal> arbitrary() {
            return gen::map(
                gen::inRange<long>(-100000000L, 100000000L),
                [](long f){return Decimal(f) / 100000;}
            );
        }
    };

    template <>
    Gen<Decimal> gen::positive();

    template<>
    struct Arbitrary<better::BPMAtBeat> {
        static Gen<better::BPMAtBeat> arbitrary() {
            return gen::construct<better::BPMAtBeat>(
                gen::positive<Decimal>(),
                gen::nonNegative<Fraction>()
            );
        }
    };

    struct TimingParams {
        std::vector<better::BPMAtBeat> events;
        Decimal offset;
    };

    template<>
    struct Arbitrary<TimingParams> {
        static Gen<TimingParams> arbitrary() {
            return gen::construct<TimingParams>(
                gen::withSize([](std::size_t size) {
                    return gen::uniqueBy<std::vector<better::BPMAtBeat>>(
                        std::max(std::size_t(1), size),
                        gen::arbitrary<better::BPMAtBeat>(),
                        [](const better::BPMAtBeat& b){return b.get_beats();}
                    );
                }),
                gen::arbitrary<Decimal>()
            );
        }
    };

    template<>
    struct Arbitrary<better::Timing> {
        static Gen<better::Timing> arbitrary() {
            return gen::map(
                gen::arbitrary<TimingParams>(),
                [](const TimingParams& t){
                    return better::Timing{t.events, t.offset};
                }
            );
        }
    };

    template<>
    struct Arbitrary<Hakus> {
        static Gen<Hakus> arbitrary() {
            return gen::container<std::set<Fraction>>(gen::positive<Fraction>());
        }
    };

    template<class T>
    struct Arbitrary<std::optional<T>> {
        static Gen<std::optional<T>> arbitrary() {
            return gen::mapcat(
                gen::arbitrary<bool>(),
                 [](bool engaged) {
                    if (not engaged) {
                        return gen::construct<std::optional<T>>();
                    } else {
                        return gen::construct<std::optional<T>>(gen::arbitrary<T>());
                    }
                }
            );
        }
    };

    template<>
    struct Arbitrary<better::Chart> {
        static Gen<better::Chart> arbitrary() {
            return gen::construct<better::Chart>(
                gen::arbitrary<std::optional<Decimal>>(),
                gen::construct<std::optional<std::shared_ptr<better::Timing>>>(
                    gen::makeShared<better::Timing>(gen::arbitrary<better::Timing>())
                ),
                gen::arbitrary<std::optional<Hakus>>(),
                gen::construct<std::shared_ptr<better::Notes>>(
                    gen::makeShared<better::Notes>(gen::arbitrary<better::Notes>())
                )
            );
        }
    };

    template<>
    struct Arbitrary<better::PreviewLoop> {
        static Gen<better::PreviewLoop> arbitrary() {
            return gen::construct<better::PreviewLoop>(
                gen::positive<Decimal>(),
                gen::positive<Decimal>()
            );
        }
    };

    Gen<std::string> ascii_string();

    template<>
    struct Arbitrary<better::Metadata> {
        static Gen<better::Metadata> arbitrary() {
            return gen::mapcat(
                gen::arbitrary<bool>(),
                 [](bool use_preview_file) {
                    if (use_preview_file) {
                        return gen::construct<better::Metadata>(
                            ascii_string(),  // title
                            ascii_string(),  // artist
                            ascii_string(),  // audio
                            ascii_string(),  // jacket
                            gen::construct<better::PreviewLoop>(),
                            gen::nonEmpty(ascii_string()),  // preview_file
                            gen::just(use_preview_file)
                        );
                    } else {
                        return gen::construct<better::Metadata>(
                            gen::arbitrary<std::string>(),  // title
                            gen::arbitrary<std::string>(),  // artist
                            gen::arbitrary<std::string>(),  // audio
                            gen::arbitrary<std::string>(),  // jacket
                            gen::arbitrary<better::PreviewLoop>(),
                            gen::just(""),  // preview_file
                            gen::just(use_preview_file)
                        );
                    }
                }
            );
        }
    };

    template<>
    struct Arbitrary<better::Song> {
        static Gen<better::Song> arbitrary() {
            return gen::construct<better::Song>(
                gen::arbitrary<std::map<std::string, better::Chart, better::OrderByDifficultyName>>(),
                gen::arbitrary<better::Metadata>(),
                gen::makeShared<better::Timing>(gen::arbitrary<better::Timing>()),
                gen::arbitrary<std::optional<Hakus>>()
            );
        }
    };
}