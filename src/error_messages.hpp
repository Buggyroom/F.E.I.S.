#pragma once

#include <filesystem>

void crash_if_missing(const std::filesystem::path& file);